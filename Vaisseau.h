#ifndef VAISSEAU_INCLUDED
#define VAISSEAU_INCLUDED
#include <SDL/SDL.h>

#define largeur 800
#define hauteur 600
#define largeur_ajoute 100
#define hauteur_ajoute 100
#define FPS 30 // frame per second, ou aussi nbre d'iteration maximal par seconde
#define duree_iteration 1000/FPS
#define constante_ajoute_a_angle 10
#define constante_de_bordure 0.75
#define vitesse_maximale 20
#define iteration_intermediaire 60
#define iteration_maximal 180
#define nbre_iteration_clinauttement 60




typedef struct point
{
    int x;
    int y;
}point;

typedef struct direction
{
    double a;
    double b;
    double c;
}direction;

typedef struct Vaisseau
{
    SDL_Surface *image_en_repos;
    SDL_Surface *image_en_mouvement;
    SDL_Surface *image_modifier1;
    SDL_Surface *image_modifier2;
    SDL_Rect position_image;
    point position_centre;
    point position_sommet;
    point point_de_la_direction;
    direction droite;
    int boolean_de_flamme;
    int angle;
    int angle_simplifie;
    int calcul_de_la_droite_demandee;
    int tourner_sens_directe;
    int tourner_sens_indirecte;
    int accelerer;
    int deccelerer;
    int angle_deceleration;
    int x_a_ajouter;
    int y_a_ajouter;
    int vitesse;
    int nbre_iteration;
    int boolean1;
    int boolean2;
    int nbre_fois_pour_recalculer_direction;
    direction coefficient_acceleration;
    int compteur;
}Vaisseau;





void calcul_x_a_ajouter_y_a_ajouter(int choix,Vaisseau* Vaisseau_joueur,int* x_a_ajouter,int * y_a_ajouter);
void initialiser_vaisseau(Vaisseau* Vaisseau_joueur);
void calcul_position_Vaisseau_joueur(int mode_de_choix,Vaisseau* Vaisseau_joueur);
void calcul_direction_vaisseau(Vaisseau* vaisseau_joueur);


#endif // VAISSEAU_INCLUDED
