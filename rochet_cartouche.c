
#include "rochet_cartouche.h"


 int Min(int a,int b){if (a<=b) return a;return b;}
int Max(int a,int b){if (a>=b) return a;return b;}

SDL_Surface* li(char* n) //pour charger les image  et les rendre transparente
{
    SDL_Surface* ld = NULL;
    SDL_Surface* oi =NULL;
    ld=IMG_Load(n);

    if (ld!=NULL)
    {
        oi=SDL_DisplayFormat(ld);
        SDL_FreeSurface(ld);
    }
   Uint32 c = SDL_MapRGB( oi->format,0 ,0xff , 0xff );
  SDL_SetColorKey (oi,SDL_SRCCOLORKEY,c);

    return oi;
}

Noeudr * nieme (Noeudr* List,int n)  //retourne l'adress du nieme Noeudr
{
    int i;
    Noeudr* p=List;

    for (i=0;i<n;i++)
    {
         if (p!=NULL)
        p=p->suiv;
    }
    return p;
}
void d_Noeudr(Noeudr** N)// destruction noeudr
{
    Noeudr* p;
if ((*N)!=NULL)
{ p=(*N)->suiv;
SDL_FreeSurface((*N)->obj.img);
free (*N);
(*N)=p;
}
}
void i_rock(Auobj* s,char* n,int lvl) // initialisation des rocher
{
     s->img=IMG_Load(n);
        Uint32 c = SDL_MapRGB( s->img->format,0 ,0xff , 0xff );
  SDL_SetColorKey (s->img,SDL_SRCCOLORKEY,c);
     s->direction=rand()%15;
     s->xr=(rand())%(W)*(rand())%(T)+W*(rand())%(2)*(rand())%(T)/2;
     s->yr=(rand())%(H)*(rand())%(T)+H*(rand())%(2)*(rand())%(T)/2;


      s->v=75+lvl*20;
    s->t=0;

}

void d_rock (Noeudr** N,int* cr,int d,char*n1,char*n2)//  division ou destruction des asteroid
{


    Noeudr *p;

    if ((*N)->obj.t==0)
    {
        p=(Noeudr*)malloc(sizeof(Noeudr));
        (*N)->obj.xr+=((*N)->obj.img->w/2);
        (*N)->obj.yr+=((*N)->obj.img->h/2);
        SDL_FreeSurface((*N)->obj.img);
        (*N)->obj.img=li(n1);
        (*N)->obj.xr-=(*N)->obj.img->w/2;
        (*N)->obj.yr-=(*N)->obj.img->h/2;
        p->obj.xr=(*N)->obj.xr;
        p->obj.yr=(*N)->obj.yr;
        (*N)->obj.direction+=d+1;
        (*N)->obj.v+=10;
        (*N)->obj.t=1;
        p->obj.img=li(n1);
        p->obj.direction=(*N)->obj.direction-2;
        p->obj.v=(*N)->obj.v;
        p->obj.t=1;
        p->suiv=(*N);
        (*N)=p;
        *cr=*cr+1;

    }
    else if ((*N)->obj.t==1)
    {
        p=(Noeudr*)malloc(sizeof(Noeudr));
        (*N)->obj.xr+=((*N)->obj.img->w/2);
        (*N)->obj.yr+=((*N)->obj.img->h/2);
        SDL_FreeSurface((*N)->obj.img);
        (*N)->obj.img=li(n2);
        (*N)->obj.xr-=(*N)->obj.img->w/2;
        (*N)->obj.yr-=(*N)->obj.img->h/2;
        p->obj.xr=(*N)->obj.xr;
        p->obj.yr=(*N)->obj.yr;
        (*N)->obj.direction+=d+1;
        (*N)->obj.v+=10;
        (*N)->obj.t=2;
        p->obj.img=li(n2);
        p->obj.direction=(*N)->obj.direction-2;
        p->obj.v=(*N)->obj.v;
        p->obj.t=2;
        p->suiv=(*N);
        (*N)=p;
        *cr=*cr+1;

    }
    else if((*N)->obj.t==2) {
         d_Noeudr(N);
        *cr=*cr-1;

    }
}
void ajout_rocher (Noeudr** Lo,char* n,int lvl)// ajoute un asteroid
{
    Noeudr* p=(*Lo);
    (*Lo)=(Noeudr*)malloc(sizeof (Noeudr));
    (*Lo)->suiv=p;
    i_rock(&((*Lo)->obj),n,lvl);

}

void ajout_cartouche (Filec* f,Auobj V1,int t)//cree et ajoute une cartouche a la file
{
    Noeudc* p;

p=(Noeudc*)malloc(sizeof(Noeudc));

i_cartouche(&(p->obj),V1,"stuff/c2.png",t);
enfiler_cartouche(f,p);

}
void enfiler_cartouche(Filec* f,Noeudc* N)
{
  if (N!=NULL)
   {N->suiv=NULL;
   N->prec=f->fin;
   if(f->deb!=NULL)
   f->fin->suiv=N;
   else
   f->deb=N;
   f->fin=N; }

}
Noeudc* defiler_cartouche(Filec* f)
{
    Noeudc* p=f->deb;
    if(f->deb!=NULL)
    f->deb=p->suiv;
    if(f->deb!=NULL)
    f->deb->prec=NULL;
    else
        f->fin=NULL;


    return p;
}
void detr_Noeudc (Noeudc* p)
{
 if (p!=NULL)
     {SDL_FreeSurface(p->obj.img);
    free(p);}
}
void affiche_cart(Filec f,SDL_Surface* screen)

{ Noeudc* p=f.deb;

while (p!=NULL)
    {
        p_Auobj(&(p->obj),screen);
        p=p->suiv;

    }
}
void i_cartouche (Auobj* s,Auobj r,char * n, int t)// initialisation des cartouche
{
     s->img=IMG_Load("stuff/c2.png");
     s->direction=r.direction;
        s->xr=r.x+r.img->w/2;
     s->yr=r.y+r.img->h/2;

      s->v=600;
    s->t=t;
}
void i_cart_en (Auobj* s,Auobj* r,char * n,int t,int lvl) //initialisation de la cartouche de l'enemie
{
    s->img=IMG_Load(n);
       Uint32 c = SDL_MapRGB( s->img->format,0 ,0xff , 0xff );
  SDL_SetColorKey (s->img,SDL_SRCCOLORKEY,c);
     s->direction=rand()%16-4;
     s->xr=r->x+r->img->w;
     s->yr=r->y+r->img->h/2;
     s->x=s->xr;
     s->y=s->yr;

      s->v=150+lvl*5;
    s->t=t;
}
void p_Auobj (Auobj* obj,SDL_Surface* screen)// positionemant d'un objet
{
    SDL_Rect d;

obj->xr+=obj->v*cos((obj->direction*M_PI/16))/Dv;
obj->yr+=obj->v*sin((obj->direction*M_PI/16))/Dv;

obj->x=((int)obj->xr%(W+T)+W+T)%(W+T);
obj->y=((int)obj->yr%(H+T)+H+T)%(H+T);

d.x=obj->x-T;
d.y=obj->y-T;

SDL_BlitSurface(obj->img,0,screen,&d);

}
void i_enemy (Auobj* s,char* n,int lvl,int t)// initialisation enemy
{

    s->img=IMG_Load(n);
       Uint32 c = SDL_MapRGB( s->img->format,0 ,0xff , 0xff );
  SDL_SetColorKey (s->img,SDL_SRCCOLORKEY,c);
     s->direction=rand()%16-4;
     s->xr=s->direction*(W+T);
     s->yr=rand()%H;
     s->x=s->xr;
     s->y=s->yr;

      s->v=rand()%50+75+lvl*10;
    s->t=t;

}

void init_Auobj(Noeudr** Lr,Filec* Fc,Auobj* ca,Auobj* en,char ** Pics,int* cr,int t,int lvl)// initialise tout les element de type Auobj
{
       int i;
        *cr=Cr+lvl/2;
        for (i=0;i<*cr;i++)
    {
        srand(time(NULL)+i);
        ajout_rocher(Lr,Pics[0],lvl);
    }
    i_enemy(en,Pics[3],lvl,t);
    i_cart_en(ca,en,Pics[4],t,lvl);
    Fc->deb=NULL;
    Fc->fin=NULL;
}
void affiche_score (int score,SDL_Surface* screen)
{
   SDL_Rect d;
    SDLPango_Context *context=SDLPango_CreateContext();
      SDLPango_SetDefaultColor(context, MATRIX_TRANSPARENT_BACK_WHITE_LETTER);
       SDLPango_SetMinimumSize(context, 640, 0);

         int margin_x = 10;
    int margin_y = 10;
    char str [6];
    int w1 = SDLPango_GetLayoutWidth(context);
    int h1 = SDLPango_GetLayoutHeight(context);
    SDL_Surface *surface = SDL_CreateRGBSurface(SDL_SWSURFACE,w1+margin_x*40,h1+margin_y*40,32,(Uint32)(255 << (8 * 3)), (Uint32)(255 << (8 * 2)),(Uint32)(255 << (8 * 1)), 255);
sprintf(str, "%d",score);


    SDLPango_SetMarkup(context,str,-1);
      SDLPango_Draw(context,surface,margin_x,margin_y);
d.x=0;
d.y=0;

       SDL_BlitSurface(surface,NULL,screen,&d);



}

void affiche_vie(int vie,SDL_Surface* screen)// affiche le nombre de vie restant

{
    int i;
    SDL_Rect d;
    SDL_Surface* img_vaisseau;
    d.y=30;
    d.x=10;
;
    img_vaisseau=rotozoomSurface(li("stuff/ship en repos.png"),0,0.7,0);
    for (i=0;i<vie;i++)
{
    SDL_BlitSurface(img_vaisseau,0,screen,&d);
    d.x+=img_vaisseau->w+2;
}
}
