
#include "rochet_cartouche.h"
#define FPS 30
#define E 5

int col_en_ro1(Auobj s,Auobj r)// partie a repeter dans la fonction col_en_ro
{
    int i,j;
    Uint32* p1=(Uint32*)(s.img->pixels);
    Uint32* p2=(Uint32*)(r.img->pixels);

   if (p1!=NULL&&p2!=NULL)
   {

   for (i=Max(0,r.x-s.x);i<Min(s.img->w,r.img->w-s.x+r.x);i++)
     {

       for(j=0;j<Min(s.img->h,r.img->h-s.y+r.y);j++)
    {
     if (p1[j*s.img->w+i]!=p1[0])
     {
        if (p2[(s.y+j-r.y)*r.img->w+s.x-r.x+i]!=p2[0])
        {
        return 1;
        }
}}}}return 0;}



int col_en_ro(Auobj s, Auobj r)// gestion de la collision entre enemie et asteroid, verfification de l'intersection puis appel de col_en_ro1 selon le positionement des 2 objet
{if((s.x+s.img->w)>=r.x&&s.x<=(r.x+r.img->w)&&((s.y+s.img->h)>=r.y&&s.y<=(r.y+r.img->h)))
     {if (s.y>=r.y)
        return (col_en_ro1(s,r));
      else if(s.y<=r.y) return (col_en_ro1(r,s));
    }
return 0;
}
int col_ca_ro(Auobj s, Auobj r)// collision cartouche asteroide ( ou bien tout autre element de type Auobj)
{int i,j,k,l;

    Uint32* p1=(Uint32*)(r.img->pixels);


    if ((s.x>=r.x&&s.y>=r.y&&s.x<=(r.x+r.img->w)&&s.y<=(r.y+r.img->h)))
    {for (i=0;i<r.img->w;i++)
    for (j=0;j<r.img->h;j++)
    {
        for (k=-3;k<=3;k++)
            for (l=-3;l<=3;l++)
        {
            if((r.x+i+k==s.x)&&(s.y==r.y+j+l))
        {


        if (p1[j*r.img->w+i]!=p1[0])
        {
          return 1;
        }

        }
    }}
    if ((s.x>=r.x+E&&s.y>=r.y+E&&s.x<=(r.x+r.img->w-E)&&s.y<=(r.y+r.img->h-E)))
    return 1;
    }

return 0;

}

int col_sh_ro1(Auobj s,Auobj r) // vue que rotozoom renvoi ( lors de l'appel dans main ) un pointeur NULL j'ai du cree col_sh_ro1 et col_sh_ro2 pour contrer le probleme
{
    int i,j;
    Uint32* p1=(Uint32*)(rotozoomSurface(li("stuff/ship en repos.png"),(-s.direction*180/16-90),1,0)->pixels);
    Uint32* p2=(Uint32*)(r.img->pixels);

   if (p1!=NULL&&p2!=NULL)
   {

   for (i=Max(0,r.x-s.x);i<Min(s.img->w,r.img->w-s.x+r.x);i++)
     {

       for(j=0;j<Min(s.img->h,r.img->h-s.y+r.y);j++)
    {
     if (p1[j*s.img->w+i]!=p1[0])
     {
        if (p2[(s.y+j-r.y)*r.img->w+s.x-r.x+i]!=p2[0])
        {

        return 1;
        }
     }}}}
    return 0;

}
int col_sh_ro2(Auobj s,Auobj r)
{
    int i,j;
    Uint32* p2=(Uint32*)(rotozoomSurface(li("stuff/ship en repos.png"),(-s.direction*180/16-90),1,0)->pixels);
    Uint32* p1=(Uint32*)(s.img->pixels);

   if (p1!=NULL&&p2!=NULL)
   {

   for (i=Max(0,r.x-s.x);i<Min(s.img->w,r.img->w-s.x+r.x);i++)
     {

       for(j=0;j<Min(s.img->h,r.img->h-s.y+r.y);j++)
    {
     if (p1[j*s.img->w+i]!=p1[0])
     {




        if (p2[(s.y+j-r.y)*r.img->w+s.x-r.x+i]!=p2[0])
        {

        return 1;
        }


    }}}}
    return 0;

}

int col_sh_ro(Auobj s, Auobj r)// appel de col_sh_ro1 et col_sh_ro2 pour gerer la collision entre vaisseau et asteroid ( ou bien tout autre element de type Auobj)
{
if((s.x+s.img->w)>=r.x&&s.x<=(r.x+r.img->w)&&((s.y+s.img->h)>=r.y&&s.y<=(r.y+r.img->h)))
     {if (s.y>=r.y)
        return (col_sh_ro1(s,r));
      else if(s.x<=r.x&&s.y<=r.y) return (col_sh_ro2(r,s));
     }

    return 0;
}
int collision (Auobj V1,Noeudr** N,int *cr,Auobj* en, Auobj *ca,Filec * f,char** Pics,int t,int t2,int lvl,int* score) // gestion de collision et ses consequence
{
    int x=0;
    Noeudr** p=N;
    int r=1;

    Noeudc* Nc=f->deb,*Nc2=NULL;
    while (p!=NULL&&(*p)!=NULL)
    {

       if (col_ca_ro(*ca,(*p)->obj))
          {
              SDL_FreeSurface(ca->img);
              i_cart_en(ca,en,Pics[4],t,lvl);
              d_rock(p,cr,ca->direction,Pics[1],Pics[2]);
          }
         if (*p!=NULL&&col_en_ro(*en,((*p)->obj)))
         {
             SDL_FreeSurface(en->img);
             i_enemy(en,Pics[3],lvl,t);
             d_rock(p,cr,en->direction,Pics[1],Pics[2]);

         }
         if (*p!=NULL&&col_sh_ro(V1,(*p)->obj)&&t-t2>2*FPS)
         {
              switch ((*p)->obj.t)
              {case 0:*score+=20;break;
              case 1:*score+=50;break;
              case 2:*score+=100;break;
              default : *score+=0;}
              d_rock(p,cr,en->direction,Pics[1],Pics[2]);
              x=1;

         }

         while (Nc!=NULL&&f->deb!=Nc2)
         {
             {Nc=defiler_cartouche(f);}
           if(r)
             {Nc2=Nc;r=0;}
             if (*p!=NULL&&Nc!=NULL&&col_ca_ro((Nc->obj),(*p)->obj))
             {
                   switch ((*p)->obj.t)
              {case 0:*score+=20;break;
              case 1:*score+=50;break;
              case 2:*score+=100;break;
              default : *score+=0;}

                  d_rock(p,cr,Nc->obj.direction,Pics[1],Pics[2]);
                  SDL_FreeSurface(Nc->obj.img);
                  free(Nc);
                  Nc=NULL;
             }
                 enfiler_cartouche(f,Nc);
         }
           r=1;Nc2=NULL;


   if (*p!=NULL)
        p=&((*p)->suiv);

    }
         if (col_sh_ro(V1,*ca)&&t-t2>2*FPS)
         {
             SDL_FreeSurface(ca->img);
             i_cart_en(ca,en,Pics[4],t,lvl);
             x=1;

         }
         if (col_sh_ro(V1,*en)&&t-t2>2*FPS)
         {
             SDL_FreeSurface(en->img);
             i_enemy(en,Pics[3],lvl,t);
             x=1;
             *score+=200;
         }

         while (Nc!=NULL&&f->deb!=Nc2)
         {
             {Nc=defiler_cartouche(f);}
             if(r)
             {Nc2=Nc;r=0;}
             if (Nc!=NULL&&col_ca_ro((Nc->obj),*en))
             {
                  *score+=200;
                  SDL_FreeSurface(en->img);
                  i_enemy(en,Pics[3],lvl,t);
                  SDL_FreeSurface(Nc->obj.img);
                  free(Nc);
                  Nc=NULL;
              }
             enfiler_cartouche(f,Nc);
         }
return x;

}
