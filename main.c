
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include <time.h>
#include <SDL/SDL_rotozoom.h>
#include <SDL_Pango.h>
#include <malloc.h>
#include "rochet_cartouche.h"
#include "Vaisseau.h"

#define V 3 //nombre de vie restante au debut du jeux





void conversion (Auobj* V1,Vaisseau Vaisseau_joueur)  //conversion de la structure vaisseau pour l'utilisation comme Auobj
{
    V1->img=Vaisseau_joueur.image_en_repos;
                        V1->x=Vaisseau_joueur.position_image.x;
                        V1->y=Vaisseau_joueur.position_image.y;
                        V1->direction=(-Vaisseau_joueur.angle*16/180)%32-8;
}


int main()
{
    int lvl=0,cr=0,t=0,i,t2=0,menu=0,vie=V,score=0,pause=0; // lvl contiendra le niveau, cr contiendra le nombre d'asteroid restant,t le temp, t2 utiliser pour limiter le temp d'immortaliter du vaisseau apres sa mort,menu  permetra de switcher entre menu du jeux , menu gameover et le jeux luimeme,vie nimbre de vie restant, score contiendra le score accumuler
    Noeudr *Lr=NULL; //Lr liste des rocher
    Auobj ca,en,V1; // ca cartouche enemy, en lenemie , l'Auobj qui servira dans la fonction collision au lieu du vaisseau joueur
    Filec Fc; // file des cartouche


    char* Pics[5] ={"stuff/r1.png","stuff/r2.png","stuff/r3.png","stuff/en.png","stuff/c2.png"};//tableau contenant l'emplacement des image



    if (SDL_Init(SDL_INIT_EVERYTHING)==-1)
    {
        printf ("pb = SDL_init");
        return 0;
    }





     Vaisseau Vaisseau_joueur;
    SDL_Surface *ecran_affichable = NULL,*ecran_virtuel = NULL,*imgmenu;
    SDL_Rect position_ecran_virtuel;
    SDL_Event e;
    int continuer = 1,x_a_ajouter=0,y_a_ajouter=0,mourir=0;

    ecran_affichable= SDL_SetVideoMode(largeur, hauteur, 32,SDL_SWSURFACE);
    ecran_virtuel = SDL_CreateRGBSurface(SDL_SWSURFACE, largeur+largeur_ajoute, hauteur+hauteur_ajoute, 32, 0, 0, 0, 0);

 SDL_WM_SetCaption("Auobj",NULL);

 Vaisseau_joueur.image_en_repos = li("stuff/ship en repos.png");
    Vaisseau_joueur.image_en_mouvement = li("stuff/ship en mouvement.png");

    Vaisseau_joueur.image_modifier1 = rotozoomSurface(Vaisseau_joueur.image_en_repos, 0, 1.0, 1);
    Vaisseau_joueur.image_modifier2 = rotozoomSurface(Vaisseau_joueur.image_en_mouvement, 0, 1.0, 1);
    initialiser_vaisseau(&Vaisseau_joueur);
    Vaisseau_joueur.compteur = 0;
    position_ecran_virtuel.x = largeur_ajoute;
    position_ecran_virtuel.y = hauteur_ajoute;
    position_ecran_virtuel.w = largeur;
    position_ecran_virtuel.h = hauteur;




imgmenu=IMG_Load("stuff/asteroids.jpg");


    while (continuer)
    {

        if (menu==0)
       {
            SDL_BlitSurface(imgmenu,NULL,ecran_affichable,NULL);
           while (SDL_PollEvent(&e))
           {
            if (e.type==SDL_MOUSEBUTTONUP)
            {
                 if ((e.button.button == SDL_BUTTON_LEFT)&&(e.button.x >294)&&(e.button.y >380)&&(e.button.x<506)&&(e.button.y<416))
            {SDL_Quit();return 0;}


        if ((e.button.button == SDL_BUTTON_LEFT)&&(e.button.x >305) &&(e.button.y >305)&&(e.button.x<509)&&(e.button.y<326))
            {menu=1;
            lvl=0;
             t=0;
              t2=0;
              init_Auobj (&Lr,&Fc,&ca,&en,Pics,&cr,t,lvl);vie=V;}}}
          }

         if (menu==1)

            {
                printf ("niveau = %d\n",lvl);
                if (cr==0&&(en.x>W+T||en.y>H+T||en.x<T||en.y<T))
            {
              lvl++;
              t=0;
              t2=0;
              init_Auobj (&Lr,&Fc,&ca,&en,Pics,&cr,t,lvl);
            }


        t++;



                  conversion(&V1,Vaisseau_joueur);



        while(SDL_PollEvent(&e))
        {
            switch(e.type)
            {
                case SDL_QUIT:continuer = 0;break;
                case SDL_KEYDOWN:
                {
                    if (e.key.keysym.sym == SDLK_RIGHT)
                    {
                        Vaisseau_joueur.nbre_fois_pour_recalculer_direction=0;
                        Vaisseau_joueur.tourner_sens_indirecte =1;
                    }
                    if (e.key.keysym.sym == SDLK_LEFT)
                    {
                        Vaisseau_joueur.nbre_fois_pour_recalculer_direction=0;
                        Vaisseau_joueur.tourner_sens_directe =1;
                    }
                    if(e.key.keysym.sym == SDLK_UP)
                    {
                        Vaisseau_joueur.accelerer = 1;
                        Vaisseau_joueur.deccelerer = 0;

                        Vaisseau_joueur.boolean1 = 1;
                        if(Vaisseau_joueur.nbre_iteration<=10)
                        {
                            Vaisseau_joueur.nbre_iteration=40;
                            Vaisseau_joueur.vitesse=5;
                        }
                    }
                    if (e.key.keysym.sym == SDLK_SPACE)
                    {
                       conversion(&V1,Vaisseau_joueur);
                        ajout_cartouche(&Fc,V1,t);}



                }break;
                case SDL_KEYUP:
                {
                    if(e.key.keysym.sym == SDLK_UP)
                    {

                        Vaisseau_joueur.accelerer = 0;
                        Vaisseau_joueur.deccelerer = 1;
                        Vaisseau_joueur.boolean2= 1;
                        Vaisseau_joueur.angle_deceleration = Vaisseau_joueur.angle_simplifie;
                    }
                    if(e.key.keysym.sym == SDLK_RIGHT)
                    {
                        Vaisseau_joueur.tourner_sens_indirecte = 0;
                        Vaisseau_joueur.calcul_de_la_droite_demandee =1;
                        if((Vaisseau_joueur.nbre_iteration<=10)&&(Vaisseau_joueur.accelerer==1))
                        {
                            Vaisseau_joueur.nbre_iteration=40;
                            Vaisseau_joueur.vitesse=5;
                        }
                    }
                    if(e.key.keysym.sym == SDLK_LEFT)
                    {
                        Vaisseau_joueur.tourner_sens_directe = 0;
                        Vaisseau_joueur.calcul_de_la_droite_demandee =1;
                        if((Vaisseau_joueur.nbre_iteration<=10)&&(Vaisseau_joueur.accelerer==1))
                        {
                            Vaisseau_joueur.nbre_iteration=40;
                            Vaisseau_joueur.vitesse=5;
                        }

                    }
                      if (e.key.keysym.sym == SDLK_p) pause=1;
                }break;
            }
        }

        if ((Vaisseau_joueur.tourner_sens_directe == 1) || (Vaisseau_joueur.tourner_sens_indirecte == 1))
        {
            //augmenter ou dimunier l'angle de rotation
            if ((Vaisseau_joueur.tourner_sens_directe ==1) && (Vaisseau_joueur.tourner_sens_indirecte ==0))
                Vaisseau_joueur.angle = Vaisseau_joueur.angle + constante_ajoute_a_angle;
            if ((Vaisseau_joueur.tourner_sens_indirecte ==1) && (Vaisseau_joueur.tourner_sens_directe ==0))
                Vaisseau_joueur.angle = Vaisseau_joueur.angle - constante_ajoute_a_angle;
            //limiter le nombre de fois pour l'appel de la fonction calculer direction vaisseau
            if(Vaisseau_joueur.nbre_fois_pour_recalculer_direction<=2)
            {
               Vaisseau_joueur.calcul_de_la_droite_demandee =1;
               Vaisseau_joueur.nbre_fois_pour_recalculer_direction++;
            }
            else
                Vaisseau_joueur.calcul_de_la_droite_demandee =0;
            //appel a rotorzomm
            SDL_FreeSurface(Vaisseau_joueur.image_modifier1);
            SDL_FreeSurface(Vaisseau_joueur.image_modifier2);
            Vaisseau_joueur.image_modifier1 = rotozoomSurface(Vaisseau_joueur.image_en_repos, Vaisseau_joueur.angle, 1.0, 0);
            Vaisseau_joueur.image_modifier2 = rotozoomSurface(Vaisseau_joueur.image_en_mouvement, Vaisseau_joueur.angle, 1.0, 0);
            //calcul de l'angle simplifie
            Vaisseau_joueur.angle_simplifie =Vaisseau_joueur.angle%360;
            if(Vaisseau_joueur.angle_simplifie<0)
                Vaisseau_joueur.angle_simplifie *=(-1);
            else if (Vaisseau_joueur.angle_simplifie>0)
                Vaisseau_joueur.angle_simplifie = 360 -Vaisseau_joueur.angle_simplifie;
            //changer le centre de rotation soit autour du centre de l'image soit autour du sommet
            if(Vaisseau_joueur.accelerer==1)
            {
                Vaisseau_joueur.position_centre.x=Vaisseau_joueur.position_sommet.x-sin(Vaisseau_joueur.angle_simplifie*M_PI/180)*Vaisseau_joueur.image_en_repos->h/2;
                Vaisseau_joueur.position_centre.y=Vaisseau_joueur.position_sommet.y+cos(Vaisseau_joueur.angle_simplifie*M_PI/180)*Vaisseau_joueur.image_en_repos->h/2;
            }
            else
            {
               Vaisseau_joueur.position_sommet.x=Vaisseau_joueur.position_centre.x+sin(Vaisseau_joueur.angle_simplifie*M_PI/180)*Vaisseau_joueur.image_en_repos->h/2;
               Vaisseau_joueur.position_sommet.y=Vaisseau_joueur.position_centre.y-cos(Vaisseau_joueur.angle_simplifie*M_PI/180)*Vaisseau_joueur.image_en_repos->h/2;
            }
            //calculer position image a partir du centre de l'image
            Vaisseau_joueur.position_image.x = Vaisseau_joueur.position_centre.x  - Vaisseau_joueur.image_modifier1->w / 2;
            Vaisseau_joueur.position_image.y = Vaisseau_joueur.position_centre.y  - Vaisseau_joueur.image_modifier1->h / 2;
            //dimunier la vitesse
            if(Vaisseau_joueur.accelerer==1)
            {
                if(Vaisseau_joueur.nbre_iteration >= 2)
                {
                    Vaisseau_joueur.nbre_iteration-=2;
                }

                else
                   Vaisseau_joueur.nbre_iteration=0;
            }
            else
            {
                if(Vaisseau_joueur.nbre_iteration >= 1)
                Vaisseau_joueur.nbre_iteration-=1;
                else
                    Vaisseau_joueur.nbre_iteration=0;
            }
        }

        if (Vaisseau_joueur.accelerer== 1)
        {
            //recalculer l'equation de  la vitesse (son variable est le nombre d'iteration )
            if(Vaisseau_joueur.boolean1==1)
            {
                Vaisseau_joueur.coefficient_acceleration.a=(double) vitesse_maximale-Vaisseau_joueur.vitesse;
                Vaisseau_joueur.coefficient_acceleration.b=(double) Vaisseau_joueur.nbre_iteration-iteration_maximal;
                Vaisseau_joueur.coefficient_acceleration.c=(double) (-1)*(Vaisseau_joueur.coefficient_acceleration.a*iteration_maximal+Vaisseau_joueur.coefficient_acceleration.b*vitesse_maximale);
                Vaisseau_joueur.boolean1=0;
            }
            Vaisseau_joueur.vitesse=(-1)*(Vaisseau_joueur.coefficient_acceleration.c+Vaisseau_joueur.coefficient_acceleration.a*Vaisseau_joueur.nbre_iteration)/Vaisseau_joueur.coefficient_acceleration.b;
            //bugggggggg
            if(Vaisseau_joueur.vitesse<0)
                Vaisseau_joueur.vitesse=0;
            //augmenter le nombre d'iteration
            if((Vaisseau_joueur.tourner_sens_directe==0)&&(Vaisseau_joueur.tourner_sens_indirecte==0))
            {

                if(Vaisseau_joueur.nbre_iteration <= iteration_maximal -2)
                Vaisseau_joueur.nbre_iteration+=2;
                else
                Vaisseau_joueur.nbre_iteration=iteration_maximal;
            }
            //calculer x a ajouter et y a ajouter dans cette iteration pour que le vaisseau se deplase a la vitesse calculer avant et dans sa direction
            calcul_x_a_ajouter_y_a_ajouter(1,&Vaisseau_joueur,&x_a_ajouter,&y_a_ajouter);
            Vaisseau_joueur.x_a_ajouter=x_a_ajouter;
            Vaisseau_joueur.y_a_ajouter=y_a_ajouter;
            //calculer la nouvelle position du vaisseau en ajoutant x_a_ajouter et y_a_ajouter
            calcul_position_Vaisseau_joueur(1,&Vaisseau_joueur);
        }

        if (Vaisseau_joueur.deccelerer ==1)
        {
            //recalculer l'equation de  la vitesse (son variable est le nombre d'iteration )
            if(Vaisseau_joueur.boolean2==1)
            {
                Vaisseau_joueur.coefficient_acceleration.a=(double) -Vaisseau_joueur.vitesse;
                Vaisseau_joueur.coefficient_acceleration.b=(double) Vaisseau_joueur.nbre_iteration;
                Vaisseau_joueur.coefficient_acceleration.c=(double) 0;
                Vaisseau_joueur.boolean2=0;
            }
            //bugggggggg
            if(Vaisseau_joueur.coefficient_acceleration.b!=0)
            Vaisseau_joueur.vitesse=(-1)*(Vaisseau_joueur.coefficient_acceleration.a*Vaisseau_joueur.nbre_iteration)/Vaisseau_joueur.coefficient_acceleration.b;
            //dimunier le nombre d'iteration
            if(Vaisseau_joueur.nbre_iteration >0)
                Vaisseau_joueur.nbre_iteration--;
            //calculer x a ajouter et y a ajouter dans cette iteration pour que le vaisseau se deplase a la vitesse calculer avant et dans sa direction
            calcul_x_a_ajouter_y_a_ajouter(1,&Vaisseau_joueur,&x_a_ajouter,&y_a_ajouter);
            Vaisseau_joueur.x_a_ajouter=x_a_ajouter;
            Vaisseau_joueur.y_a_ajouter=y_a_ajouter;
            //calculer la nouvelle position du vaisseau en ajoutant x_a_ajouter et y_a_ajouter
            calcul_position_Vaisseau_joueur(0,&Vaisseau_joueur);
        }
        if((mourir==1)&&(Vaisseau_joueur.compteur==0))
        {
            //initialiser le vaisseau comme au debut du jeu une seule fois
            Vaisseau_joueur.image_modifier1 = rotozoomSurface(Vaisseau_joueur.image_en_repos, 0, 1.0, 1);
            Vaisseau_joueur.image_modifier2 = rotozoomSurface(Vaisseau_joueur.image_en_mouvement, 0, 1.0, 1);
            initialiser_vaisseau(&Vaisseau_joueur);

        }
        //PARTIE AFFICHAGE


            //afficher le vaisseau joueur en repos et le clinauttement si existe
            if(Vaisseau_joueur.accelerer == 0)
            {
                if(mourir==1)
                {
                    if((Vaisseau_joueur.compteur!=nbre_iteration_clinauttement)&&(Vaisseau_joueur.compteur%5!=0))
                       SDL_BlitSurface(Vaisseau_joueur.image_modifier1 , NULL, ecran_virtuel, &Vaisseau_joueur.position_image);
                       Vaisseau_joueur.compteur++;
                }else
                SDL_BlitSurface(Vaisseau_joueur.image_modifier1 , NULL, ecran_virtuel, &Vaisseau_joueur.position_image);
            }
            else
            //afficher la flamme lorsque le joueur accelere et le clinauttement si existe
            {
                if(Vaisseau_joueur.boolean_de_flamme == 1)
                {
                    if(mourir==1)
                    {
                        if((Vaisseau_joueur.compteur!=nbre_iteration_clinauttement)&&(Vaisseau_joueur.compteur%5!=0))
                            SDL_BlitSurface(Vaisseau_joueur.image_modifier2 , NULL, ecran_virtuel, &Vaisseau_joueur.position_image);
                            Vaisseau_joueur.compteur++;
                    }else
                    SDL_BlitSurface(Vaisseau_joueur.image_modifier2 , NULL, ecran_virtuel, &Vaisseau_joueur.position_image);
                    Vaisseau_joueur.boolean_de_flamme =0;
                }
                else
                {
                    if(mourir==1)
                    {
                        if((Vaisseau_joueur.compteur!=nbre_iteration_clinauttement)&&(Vaisseau_joueur.compteur%5!=0))
                            SDL_BlitSurface(Vaisseau_joueur.image_modifier1 , NULL, ecran_virtuel, &Vaisseau_joueur.position_image);
                            Vaisseau_joueur.compteur++;
                    }else
                    SDL_BlitSurface(Vaisseau_joueur.image_modifier1 , NULL, ecran_virtuel, &Vaisseau_joueur.position_image);
                    Vaisseau_joueur.boolean_de_flamme =1;
                }
            }
            //mettre l'evenement qui a declancher la mourt du vaisseau joueur a zero lorque le clinottement se termine
            if(Vaisseau_joueur.compteur==nbre_iteration_clinauttement)
            {
                Vaisseau_joueur.compteur=0;
                mourir=0;
            }
 SDL_BlitSurface(ecran_virtuel , &position_ecran_virtuel, ecran_affichable, 0);
    printf ("nombre asteroide = %d\n",cr);
    for (i=0;i<cr;i++)
    {
        p_Auobj(&(nieme(Lr,i)->obj),ecran_affichable);
        printf ("rock %d at x=%d,y=%d \n",i,nieme(Lr,i)->obj.x,nieme(Lr,i)->obj.y);
    }
    if ((t-en.t)>3*FPS&&t>10*FPS)
    {
        if ((t-en.t)>6*FPS){en.direction=rand()%16;en.t=t-3*FPS;}
        p_Auobj(&en,ecran_affichable);
        p_Auobj(&ca,ecran_affichable);
        if ((t-ca.t)>3*FPS) i_cart_en(&ca,&en,Pics[4],t,lvl);
    }

  if (collision(V1,&Lr,&cr,&en,&ca,&Fc,Pics,t,t2,lvl,&score)&&t-t2>2*FPS){mourir=1;t2=t;vie--;}
affiche_cart(Fc,ecran_affichable);
if(Fc.deb!=NULL&&(t-Fc.deb->obj.t)>FPS/3.5)
    detr_Noeudc(defiler_cartouche(&Fc));
  affiche_vie (vie,ecran_affichable);
  printf ("Score = %d\n",score);

  if (vie==0)
    menu=2;
    affiche_score(score,ecran_affichable);

             while(pause)
                {
                    while (SDL_PollEvent(&e))
                    {
                        if (e.type==SDL_KEYUP&&e.key.keysym.sym == SDLK_p)
                        {pause=0;}
                    }
                }

            }
            if (menu==2)
            {
                 SDL_BlitSurface(IMG_Load("stuff/GO.jpg"),NULL,ecran_affichable,NULL);
                 while (SDL_PollEvent(&e)) if (e.key.keysym.sym== SDLK_RETURN) menu=0;
            }





        SDL_Flip(ecran_affichable);
        SDL_Delay(1000/FPS);
        SDL_FillRect(ecran_virtuel, NULL, SDL_MapRGB(ecran_virtuel->format, 0,0, 0));

    }

  SDL_FreeSurface(ecran_virtuel);
    SDL_FreeSurface(Vaisseau_joueur.image_en_repos);
    SDL_FreeSurface(Vaisseau_joueur.image_en_mouvement);
    SDL_FreeSurface(Vaisseau_joueur.image_modifier1);
    SDL_FreeSurface(Vaisseau_joueur.image_modifier2);
    SDL_Quit();
    return 0;
}

