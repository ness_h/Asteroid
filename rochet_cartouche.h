#ifndef RC_H_INCLUDED
#define RC_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include <time.h>
#include <SDL/SDL_rotozoom.h>
#include <SDL_Pango.h>

#define W 800   //width (largeur)
#define H 600   //height (longueur)
#define K 2     // constante de sensibiliter
#define T 100   //constante a utiliser pour le repere virtuel
#define C 0.08    // constante de frontement
#define Dv 20 //constant pour la dimunution de la vitess des objet automatique
#define F 1.1 //servira a diminuer  le derapage
#define Cr 5 //nombre de rocher au debut du jeux

struct A {
    SDL_Surface* img;
    int x,y;    //position en valeur entiere pour etre utiliser dans la liste la colision
    float xr,yr; // position reel
    int direction,t;  //variable qui servira a stocker le temp  de creation des cartouch, temp de reinitialisation de l'enemy ou bien a identifier la taille pour les rocher
    int v;   //vitess
} ;
typedef struct A Auobj; //objet automatique, non controler par le joueur
struct Noeud1
{
    Auobj obj;
    struct Noeud1 * suiv;
};
typedef struct Noeud1 Noeudr ; //Noeud comportant un Objet automatique qui servira pour la liste de stockage  des rocher
struct Noeud2
{
     Auobj obj;
   struct Noeud2 * suiv;
   struct Noeud2 * prec;
};
typedef struct Noeud2 Noeudc ;//Noeud comportant un Objet automatique qui servira pour la file de stockage  des cartouche
 struct Fi
 {
     Noeudc* deb;
     Noeudc* fin;
};
 typedef struct Fi Filec; //file des cartouche ( structurer comme liste doublement chainee )
 int Min(int ,int );
int Max(int ,int );
 SDL_Surface* li(char* ); //pour charger les image  et les rendre transparente
Noeudr * nieme (Noeudr* ,int );
void d_Noeudr(Noeudr** );
void i_rock(Auobj* ,char* ,int ); // initialisation des rocher
void d_rock (Noeudr**,int* ,int,char*,char*);
void ajout_rocher (Noeudr** ,char*,int );
void ajout_cartouche (Filec*,Auobj,int );//cree et ajoute une cartouche a la file
void enfiler_cartouche(Filec* ,Noeudc* );
Noeudc* defiler_cartouche(Filec* );
void detr_Noeudc (Noeudc* );
void affiche_cart(Filec ,SDL_Surface* );
void i_cartouche (Auobj* ,Auobj ,char * , int );// initialisation des cartouche
void i_cart_en (Auobj* ,Auobj* ,char * ,int ,int );
void p_Auobj (Auobj* ,SDL_Surface* );
void i_enemy (Auobj* ,char* ,int ,int );
void init_Auobj(Noeudr** ,Filec* ,Auobj* ,Auobj* ,char ** ,int*,int ,int );
void affiche_vie(int ,SDL_Surface*);// affiche le nombre de vie restant
void affiche_score (int ,SDL_Surface*);
#endif // RC_H_INCLUDED
