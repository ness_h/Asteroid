#include "Vaisseau.h"
#include "math.h"




void initialiser_vaisseau(Vaisseau* Vaisseau_joueur)
{
    Vaisseau_joueur->position_image.x = largeur / 2 - Vaisseau_joueur->image_modifier1->w / 2 + largeur_ajoute;
    Vaisseau_joueur->position_image.y = hauteur / 2 - Vaisseau_joueur->image_modifier1->h / 2 + hauteur_ajoute;
    Vaisseau_joueur->position_centre.x = largeur/ 2 + largeur_ajoute;
    Vaisseau_joueur->position_centre.y = hauteur/ 2 + hauteur_ajoute;
    Vaisseau_joueur->position_sommet.x = largeur/ 2 + largeur_ajoute;
    Vaisseau_joueur->position_sommet.y = hauteur/ 2 + hauteur_ajoute - Vaisseau_joueur->image_modifier1->h / 2;
    Vaisseau_joueur->point_de_la_direction.x = largeur / 2 - Vaisseau_joueur->image_modifier1->w / 2 + largeur_ajoute;
    Vaisseau_joueur->point_de_la_direction.y = 0;
    Vaisseau_joueur->boolean_de_flamme =1;
    Vaisseau_joueur->angle =0;
    Vaisseau_joueur->angle_simplifie=0;
    Vaisseau_joueur->calcul_de_la_droite_demandee =0;
    Vaisseau_joueur->tourner_sens_directe =0;
    Vaisseau_joueur->tourner_sens_indirecte=0;
    Vaisseau_joueur->accelerer = 0;
    Vaisseau_joueur->deccelerer = 0;
    Vaisseau_joueur->angle_deceleration =0;
    Vaisseau_joueur->x_a_ajouter =0;
    Vaisseau_joueur->y_a_ajouter =0;
    Vaisseau_joueur->vitesse=0;
    Vaisseau_joueur->nbre_iteration =0;
    Vaisseau_joueur->coefficient_acceleration.a=0;
    Vaisseau_joueur->coefficient_acceleration.b=0;
    Vaisseau_joueur->coefficient_acceleration.c=0;
    Vaisseau_joueur->boolean1 = 0;
    Vaisseau_joueur->boolean2 = 0;
    Vaisseau_joueur->nbre_fois_pour_recalculer_direction = 0;
}
void calcul_x_a_ajouter_y_a_ajouter(int choix,Vaisseau* Vaisseau_joueur,int* x_a_ajouter,int * y_a_ajouter)
{
    double a=0,b=0,y=0,x=0;
    int tolerence_erreur_de_la_vitesse,vitesse;

    if(choix==1)
        vitesse=Vaisseau_joueur->vitesse;

    if (Vaisseau_joueur->vitesse>=5)
        tolerence_erreur_de_la_vitesse=0.8;
    else if (Vaisseau_joueur->vitesse>=3)
        tolerence_erreur_de_la_vitesse=0.5;
    else
        tolerence_erreur_de_la_vitesse=0.2;

    a=Vaisseau_joueur->droite.a;
    b=Vaisseau_joueur->droite.b;
    x=vitesse/sqrt(1+pow(a/b,2));
    y=(-1)*(a*x)/b;
    if(y<0)
        y*=(-1);
    if (x-floor(x)>tolerence_erreur_de_la_vitesse)
        (*x_a_ajouter)=floor(x)+1;
    else
        (*x_a_ajouter)=floor(x);
    if (y-floor(y)>tolerence_erreur_de_la_vitesse)
        (*y_a_ajouter)=floor(y)+1;
    else
        (*y_a_ajouter)=floor(y);
}

void calcul_position_Vaisseau_joueur(int choix,Vaisseau* Vaisseau_joueur)
{
    int angle,x_a_ajouter,y_a_ajouter;;
    SDL_Rect ancien_position;
    if (choix==1)
    {
            if(Vaisseau_joueur->calcul_de_la_droite_demandee==0)
                angle=Vaisseau_joueur->angle_deceleration;
            else
                angle=Vaisseau_joueur->angle_simplifie;
    }else
    {
        angle=Vaisseau_joueur->angle_deceleration;
    }

    ancien_position.x=Vaisseau_joueur->position_image.x;
    ancien_position.y=Vaisseau_joueur->position_image.y;
    if (angle == 0)
    {
        Vaisseau_joueur->position_image.y = Vaisseau_joueur->position_image.y-Vaisseau_joueur->vitesse;
    }else
    if (angle == 180)
    {
        Vaisseau_joueur->position_image.y = Vaisseau_joueur->position_image.y+Vaisseau_joueur->vitesse;
    }else
    if (angle == 90)
    {
        Vaisseau_joueur->position_image.x = Vaisseau_joueur->position_image.x+Vaisseau_joueur->vitesse;
    }else
    if (angle == 270)
    {
        Vaisseau_joueur->position_image.x = Vaisseau_joueur->position_image.x-Vaisseau_joueur->vitesse;
    }
    else
        {
            if((Vaisseau_joueur->calcul_de_la_droite_demandee == 1)&&(choix==1))
            {
                calcul_direction_vaisseau(Vaisseau_joueur);
                calcul_x_a_ajouter_y_a_ajouter(1,Vaisseau_joueur,&x_a_ajouter,&y_a_ajouter);
                Vaisseau_joueur->x_a_ajouter=x_a_ajouter;
                Vaisseau_joueur->y_a_ajouter=y_a_ajouter;
                Vaisseau_joueur->calcul_de_la_droite_demandee= 0;
                Vaisseau_joueur->angle_deceleration=Vaisseau_joueur->angle_simplifie;
            }

            if ((angle>0)&&(angle<180))
            {
                Vaisseau_joueur->position_image.x = Vaisseau_joueur->position_image.x +Vaisseau_joueur->x_a_ajouter;
            }else
            {
                Vaisseau_joueur->position_image.x = Vaisseau_joueur->position_image.x -Vaisseau_joueur->x_a_ajouter;
            }
            if ((angle>90)&&(angle<270))
            {
                Vaisseau_joueur->position_image.y = Vaisseau_joueur->position_image.y +Vaisseau_joueur->y_a_ajouter;
            }else
            {
                Vaisseau_joueur->position_image.y = Vaisseau_joueur->position_image.y -Vaisseau_joueur->y_a_ajouter;
            }
        }


    if (Vaisseau_joueur->position_image.x<=largeur_ajoute-constante_de_bordure*Vaisseau_joueur->image_modifier1->w)
    {
        Vaisseau_joueur->position_image.x=largeur + largeur_ajoute;
    }else
    if (Vaisseau_joueur->position_image.x>=largeur+largeur_ajoute)
    {
        Vaisseau_joueur->position_image.x=largeur_ajoute - constante_de_bordure*Vaisseau_joueur->image_modifier1->w;
    }
    if (Vaisseau_joueur->position_image.y<=hauteur_ajoute-constante_de_bordure*Vaisseau_joueur->image_modifier1->h)
    {
        Vaisseau_joueur->position_image.y=hauteur+hauteur_ajoute;
    }else
    if (Vaisseau_joueur->position_image.y>=hauteur+hauteur_ajoute)
    {
        Vaisseau_joueur->position_image.y=hauteur_ajoute - constante_de_bordure*Vaisseau_joueur->image_modifier1->h;
    }
    //deplacer le centre de l'image et le sommet
    Vaisseau_joueur->position_centre.x = Vaisseau_joueur->position_image.x+Vaisseau_joueur->image_modifier1->w/2;
    Vaisseau_joueur->position_centre.y = Vaisseau_joueur->position_image.y+Vaisseau_joueur->image_modifier1->h/2;
    Vaisseau_joueur->position_sommet.x=Vaisseau_joueur->position_sommet.x-(ancien_position.x-Vaisseau_joueur->position_image.x);
    Vaisseau_joueur->position_sommet.y=Vaisseau_joueur->position_sommet.y-(ancien_position.y-Vaisseau_joueur->position_image.y);
}


void calcul_direction_vaisseau(Vaisseau* Vaisseau_joueur)
{
    double distance;
    if (((Vaisseau_joueur->angle_simplifie>0) && (Vaisseau_joueur->angle_simplifie<=45)) || (( Vaisseau_joueur->angle_simplifie>180) && (Vaisseau_joueur->angle_simplifie<=225)))
    {
        distance = tan((Vaisseau_joueur->angle_simplifie%90)*M_PI/180)*Vaisseau_joueur->position_image.y;
        Vaisseau_joueur->point_de_la_direction.x = distance+Vaisseau_joueur->position_image.x;
        Vaisseau_joueur->point_de_la_direction.y = 0;
    }else
    if ((( Vaisseau_joueur->angle_simplifie>45) && (Vaisseau_joueur->angle_simplifie<90)) || (( Vaisseau_joueur->angle_simplifie>225) && (Vaisseau_joueur->angle_simplifie<270)))
    {
        distance = tan(M_PI/2-(Vaisseau_joueur->angle_simplifie%90)*M_PI/180)*Vaisseau_joueur->position_image.x;
        Vaisseau_joueur->point_de_la_direction.x = 0;
        Vaisseau_joueur->point_de_la_direction.y = distance+Vaisseau_joueur->position_image.y;
    }else
    if ((( Vaisseau_joueur->angle_simplifie>90) && (Vaisseau_joueur->angle_simplifie<=135)) || (( Vaisseau_joueur->angle_simplifie>270) && (Vaisseau_joueur->angle_simplifie<=315)))
    {
        distance = tan((Vaisseau_joueur->angle_simplifie%90)*M_PI/180)*Vaisseau_joueur->position_image.x;
        Vaisseau_joueur->point_de_la_direction.x = 0;
        Vaisseau_joueur->point_de_la_direction.y = Vaisseau_joueur->position_image.y-distance;
    }else
    if ((( Vaisseau_joueur->angle_simplifie>135) && (Vaisseau_joueur->angle_simplifie<180)) || (( Vaisseau_joueur->angle_simplifie>315) && (Vaisseau_joueur->angle_simplifie<360)))
    {
        distance = tan(M_PI/2-(Vaisseau_joueur->angle_simplifie%90)*M_PI/180)*Vaisseau_joueur->position_image.y;
        Vaisseau_joueur->point_de_la_direction.x = Vaisseau_joueur->position_image.x-distance;
        Vaisseau_joueur->point_de_la_direction.y = 0;
    }
        Vaisseau_joueur->droite.a=(double) Vaisseau_joueur->position_image.y-Vaisseau_joueur->point_de_la_direction.y;
        Vaisseau_joueur->droite.b=(double) Vaisseau_joueur->point_de_la_direction.x-Vaisseau_joueur->position_image.x;}
